class TelcoSystem {
    clients = new Map();
    rushHour;
    constructor(rushHour) {
        this.rushHour = rushHour;
    }

    dateDifferenceInMinutes = (dateInitial, dateFinal) =>
        (dateFinal - dateInitial) / 1_000/ 60;
    historicalNumberOfCalls() {
        let number = 0;
        this.clients.forEach((v)  => {
            number += v.totalcalls;
        });
        return number;
    }

    historicalTotalBilled() {
        let number = 0;
        this.clients.forEach((v)  => {
            number += v.totalBilled;
        });
        return number;
    }

    registerNationalCallBetween(aDateTime, anotherDateTime, client) {
        let clientObj = this.getClient(client);
        let duration = this.dateDifferenceInMinutes(aDateTime, anotherDateTime);
        let bill = duration * 1.5;
        clientObj.addCall(new Call('national', duration, bill));
        this.clients.set(client, clientObj)
    }

    registerInternationalCallBetween(aDateTime, anotherDateTime, client) {
        let clientObj = this.getClient(client);
        let duration = this.dateDifferenceInMinutes(aDateTime, anotherDateTime);
        let bill = duration * 2;
        clientObj.addCall(new Call('national', duration, bill));
        this.clients.set(client, clientObj)
    }

    registerLocalCallBetween(aDateTime, anotherDateTime, client) {
        let clientObj = this.getClient(client);
        let duration = this.dateDifferenceInMinutes(aDateTime, anotherDateTime);
        let bill = this.happensInRushHour(aDateTime, anotherDateTime, duration);
        clientObj.addCall(new Call('national', duration, bill));
        this.clients.set(client, clientObj)
    }

    numberOfCallsFor(client) {
        let clientObj = this.getClient(client);
        return clientObj.totalcalls;
    }

    totalBilledFor(client) {
        let clientObj = this.getClient(client);
        return clientObj.totalBilled;
    }

    getClient(client){
        if(this.clients.has(client)){
            return this.clients.get(client);
        }
        return new Client();
    }

    happensInRushHour(aDateTime, anotherDateTime, duration){
        if(aDateTime.getHours() == this.rushHour && anotherDateTime.getHours() == this.rushHour){
            return duration * 1;
        }
        else if(aDateTime.getHours() == this.rushHour){
            let minutes = anotherDateTime.getMinutes();
            return ((duration-minutes) * 1) + minutes * 0.5;
        }
        else if(anotherDateTime.getHours() == this.rushHour){
            let minutes = 60 - aDateTime.getMinutes();
            return ((duration-minutes) * 1) + minutes * 0.5;
        }
        return duration * 0.5;
    }
}

class Client{
    totalcalls;
    totalBilled;
    calls = []
    constructor() {
        this.totalcalls = 0;
        this.totalBilled = 0;
    }
    addCall(call){
        this.calls.push(call);
        this.totalcalls++;
        this.totalBilled += call.bill;
    }
}

class Call {
    type;
    duration;
    bill;
    constructor(type, duration, bill) {
        this.type = type;
        this.duration = duration;
        this.bill = bill;
    }
}

module.exports = TelcoSystem;